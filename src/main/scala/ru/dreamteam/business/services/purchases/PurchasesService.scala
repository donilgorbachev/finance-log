package ru.dreamteam.business.services.purchases

import ru.dreamteam.business.Purchase.{Comment, PurchaseCategory}
import ru.dreamteam.business.services.purchases.interpreter.PurchaseInfo
import ru.dreamteam.business.{Money, Purchase, User}

trait PurchasesService[F[_]] {

  def getPurchases(userId: User.Id): F[List[Purchase]]

  def getPurchaseByCategory(userId: User.Id, purchaseCategory: PurchaseCategory): F[List[Purchase]]

  def addPurchase(
    userId: User.Id,
    money: Money,
    comment: Comment,
    purchaseCategory: PurchaseCategory
  ): F[PurchaseInfo]

  def purchaseInfo(userId: User.Id, purchaseId: Purchase.Id): F[PurchaseInfo]

}
