package ru.dreamteam.business.services.purchases.interpreter

import cats.MonadThrow
import cats.syntax.all._
import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import ru.dreamteam.business.Purchase._
import ru.dreamteam.business.repository.purchases.PurchasesRepository
import ru.dreamteam.business.repository.purchases.PurchasesRepository.PurchaseRequest
import ru.dreamteam.business.services.purchases.PurchasesService
import ru.dreamteam.business.services.users.interpreter.BusinessError
import ru.dreamteam.business.{Money, Purchase, User}

class PurchasesServiceInterpreter[F[_]: MonadThrow](repo: PurchasesRepository[F])
  extends PurchasesService[F] {
  override def getPurchases(userId: User.Id): F[List[Purchase]] = repo.findByUserId(userId)

  override def getPurchaseByCategory(
    userId: User.Id,
    purchaseCategory: PurchaseCategory
  ): F[List[Purchase]] = repo.findByCategory(userId, purchaseCategory)

  override def addPurchase(
    userId: User.Id,
    money: Money,
    comment: Comment,
    purchaseCategory: PurchaseCategory
  ): F[PurchaseInfo] = for {
    purchaseId         <- repo.addPurchase(userId, PurchaseRequest(money, comment, purchaseCategory))
    purchaseInfoOption <- repo.findByPurchaseId(userId, purchaseId)
    purchaseInfo       <-
      MonadThrow[F].fromOption(purchaseInfoOption, PurchaseNotExists("purchase by user not found"))
  } yield purchaseInfo

  override def purchaseInfo(userId: User.Id, purchaseId: Purchase.Id): F[PurchaseInfo] = for {
    purchaseOption <- repo.findByPurchaseId(userId, purchaseId)
    purchaseInfo   <-
      MonadThrow[F].fromOption(purchaseOption, PurchaseNotExists("purchase or user not found"))
  } yield PurchaseInfo(purchaseInfo.purchaseId, purchaseInfo.purchaseMoney, purchaseInfo.purchaseComment, purchaseInfo.purchaseCategory)

}

case class PurchaseNotExists(msg: String) extends BusinessError(msg)

import ru.dreamteam.infrastructure.newtype._

@derive(tethysReader, tethysWriter)
case class PurchaseInfo(
  purchaseId: Purchase.Id,
  purchaseMoney: Money,
  purchaseComment: Purchase.Comment,
  purchaseCategory: Purchase.PurchaseCategory
)
