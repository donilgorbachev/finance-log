package ru.dreamteam.business.repository.purchases.interpreter

import cats.syntax.all._
import cats.effect.BracketThrow
import doobie.ConnectionIO
import ru.dreamteam.business.{Currency, Money, Purchase, User}
import doobie.implicits._
import doobie.util.transactor.Transactor
import ru.dreamteam.business.Purchase.PurchaseCategory
import ru.dreamteam.business.repository.purchases.PurchasesRepository
import ru.dreamteam.business.repository.purchases.PurchasesRepository.PurchaseRequest
import ru.dreamteam.business.repository.purchases.interpreter.PurchaseRepositoryInterpreter.{insertPurchase, selectByCategory, selectByPurchaseId, selectByUserId, transform}
import ru.dreamteam.business.services.purchases.interpreter.{PurchaseInfo, PurchaseNotExists}
import ru.dreamteam.business.services.users.interpreter.BusinessError

class PurchaseRepositoryInterpreter[F[_]: BracketThrow](transactor: Transactor[F])
  extends PurchasesRepository[F] {

  // эти два метода надо хорошо чекнуть, нормально ли я их исправила, вроде как трансформ больше не нужен
  override def findByUserId(userId: User.Id): F[List[Purchase]] = for {
    raw <- selectByUserId(userId.id).transact(transactor)
  } yield raw.map(x => Purchase(Purchase.Id(x)))

  override def findByCategory(userId: User.Id, category: PurchaseCategory): F[List[Purchase]] =
    for {
      raw <- selectByUserId(userId.id).transact(transactor)
    } yield raw.map(x => Purchase(Purchase.Id(x)))

  override def findByPurchaseId(userId: User.Id, purchaseId: Purchase.Id): F[Option[PurchaseInfo]] =
    for {
      raw   <- selectByPurchaseId(userId.id, purchaseId.id).transact(transactor)
      result = raw.map(transform).separate match {
                 case (_, right) => right
               }
    } yield result

  override def addPurchase(userId: User.Id, purchase: PurchaseRequest): F[Purchase.Id] = for {
    id <- insertPurchase(
            userId.id,
            purchase.money.amount,
            purchase.money.currency.entryName,
            purchase.comment.comment,
            purchase.category.entryName
          ).transact(transactor)
  } yield Purchase.Id(id)

}

object PurchaseRepositoryInterpreter {

  // TODO PurchaseCategory via Either
  def transform(raw: PurchaseRaw): Either[BusinessError, PurchaseInfo] = for {
    purchaseCategory <- PurchaseCategory.withNameInsensitiveOption(raw.category).toRight(
                          PurchaseNotExists("purchase not exists")
                        )
    currency          = Currency.parse(raw.currency)
  } yield PurchaseInfo(Purchase.Id(raw.purchaseId), Money(raw.amount, currency), Purchase.Comment(raw.comment), purchaseCategory)

  def selectByUserId(userId: String): ConnectionIO[List[String]] =
    sql"SELECT id FROM purchases WHERE userId = $userId"
      .query[String]
      .to[List]

  def selectByCategory(
    userId: String,
    category: PurchaseCategory
  ): doobie.ConnectionIO[List[String]] =
    sql"SELECT id FROM purchases WHERE category = ${category.entryName} AND userId = $userId"
      .query[String]
      .to[List]

  def selectByPurchaseId(userId: String, purchaseId: String): ConnectionIO[Option[PurchaseRaw]] =
    sql"SELECT id, amount, currency, comment, category FROM purchases WHERE id = $purchaseId AND userId = $userId"
      .query[PurchaseRaw]
      .option

  def insertPurchase(
    userId: String,
    amount: BigDecimal,
    currency: String,
    comment: String,
    category: String
  ): ConnectionIO[String] =
    sql"INSERT INTO purchases (amount, currency, comment, category, userId) VALUES ($amount, $currency, $comment, $category, $userId)"
      .update
      .withUniqueGeneratedKeys[String]("id")

  case class PurchaseRaw(
    purchaseId: String,
    amount: BigDecimal,
    currency: String,
    comment: String,
    category: String
  )

}
