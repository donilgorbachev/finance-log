package ru.dreamteam.business.repository.users.interpreter

import cats.syntax.all._
import cats.effect.Sync
import ru.dreamteam.business.User
import doobie.implicits._
import ru.dreamteam.business.repository.users.UsersRepository
import ru.dreamteam.business.repository.users.UsersRepository.UserReq
import ru.dreamteam.business.repository.users.interpreter.UsersRepositoryInterpreter.{insertUser, selectUser, selectUserByLogin, transform}
import doobie.implicits.toSqlInterpolator
import doobie.util.transactor.Transactor

class UsersRepositoryInterpreter[F[_]: Sync](transactor: Transactor[F]) extends UsersRepository[F] {

  override def findUser(userId: User.Id): F[Option[User]] = for {
    rawOption <- selectUser(userId.id).transact(transactor)
    raw        = rawOption.map(transform)
  } yield raw

  override def addUser(user: UserReq): F[User.Id] = for {
    id <- insertUser(user.login.login, user.password.password).transact(transactor)
  } yield User.Id(id)

  override def findUserByLogin(userLogin: User.Login): F[Option[User]] = for {
    rawOption <- selectUserByLogin(userLogin.login).transact(transactor)
    raw        = rawOption.map(transform)
  } yield raw

}

object UsersRepositoryInterpreter {

  def transform(raw: UserRaw): User =
    User(User.Id(raw.id), User.Login(raw.login), User.Password(raw.password))

  def selectUser(userId: String): doobie.ConnectionIO[Option[UserRaw]] =
    sql"SELECT * FROM users WHERE id = $userId"
      .query[UserRaw]
      .option

  def selectUserByLogin(userLogin: String): doobie.ConnectionIO[Option[UserRaw]] =
    sql"SELECT * FROM users WHERE login = $userLogin"
      .query[UserRaw]
      .option

  // тудушка не валидная, unique, навешанный на столбец login гарантирует уникальность записанного логина
  // TODO добавить if login exist или через second primarykey; Int -> String
  def insertUser(login: String, password: String): doobie.ConnectionIO[String] =
    sql"INSERT INTO users (login, password) VALUES ($login, $password)"
      .update
      .withUniqueGeneratedKeys[String]("userId")

  case class UserRaw(
    id: String,
    login: String,
    password: String
  )

}
