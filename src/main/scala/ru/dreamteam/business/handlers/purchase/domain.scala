package ru.dreamteam.business.handlers.purchase

import derevo.derive
import derevo.tethys._
import ru.dreamteam.business.Purchase
import ru.dreamteam.business.services.purchases.interpreter.PurchaseInfo
import sttp.tapir.description

case class PurchaseInfoRequest(purchaseId: String)

@derive(tethysReader, tethysWriter)
@description("Информация о покупке")
case class PurchaseInfoResponse(info: PurchaseInfo)

case class GetPurchasesRequest()

@derive(tethysReader, tethysWriter)
@description("Получить все покупки")
case class GetPurchasesResponse(
  @description("Покупки пользователя") purchases: List[Purchase]
)

case class GetPurchaseByCategoryRequest(purchaseCategory: String)

@derive(tethysReader, tethysWriter)
@description("Получить все покупки заданного типа")
case class GetPurchaseByCategoryResponse(
  @description("Покупки пользователя") purchases: List[Purchase]
)

@derive(tethysReader, tethysWriter)
case class AddPurchaseRequest(
  amount: BigDecimal,
  currency: String,
  comment: String,
  category: String
)

@derive(tethysReader, tethysWriter)
@description("Добавление покупки")
case class AddPurchaseResponse(
  @description("Идентификатор покупки") purchase: PurchaseInfo
)
