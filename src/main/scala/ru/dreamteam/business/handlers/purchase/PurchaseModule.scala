package ru.dreamteam.business.handlers.purchase

import org.http4s.HttpRoutes
import ru.dreamteam.infrastructure.http.{HttpModule, Response}
import ru.dreamteam.infrastructure.{MainTask}
import sttp.tapir.{endpoint, query, Endpoint}
import sttp.tapir.generic.auto._
import sttp.tapir.json.tethysjson.jsonBody
import sttp.tapir.server.http4s.{Http4sServerInterpreter, Http4sServerOptions}
import zio._
import ru.dreamteam.business.handlers.purchase.handlers.{AddPurchaseHandler, GetPurchaseByCategoryHandler, GetPurchasesHandler, PurchaseInfoHandler}
import ru.dreamteam.business.services.purchases.PurchasesService
import ru.dreamteam.business.services.session.SessionService
import zio.interop.catz._
import zio.interop.catz.implicits._
import sttp.tapir.codec.newtype._

class PurchaseModule(purchaseService: PurchasesService[MainTask])(
  implicit
  runtime: zio.Runtime[Unit],
  sessionService: SessionService[MainTask]
) extends HttpModule[Task] {

  val purchaseInfoEndpoint = endpoint // взять одну покупку
    .get
    .in("purchase_info")
    .in(query[String]("id").mapTo(PurchaseInfoRequest.apply _))
    .out(jsonBody[Response[PurchaseInfoResponse]])
    .summary("Информация по пользователю")
    .handleWithAuthorization(req => userId => PurchaseInfoHandler(purchaseService)(req, userId))

  val getUserPurchasesEndpiont = endpoint // взять все покупки пользователя
    .get
    .in("get_purchases")
    .out(jsonBody[Response[GetPurchasesResponse]])
    .summary("Получение покупок пользователя")
    .handleWithAuthorization(_ =>
      userId => GetPurchasesHandler(purchaseService)(GetPurchasesRequest(), userId)
    )

  val getPurchasesByTypeEndpiont = endpoint // взять покупки по типу
    .get
    .in("get_purchases_by_type")
    .in(query[String]("purchaseType").mapTo(GetPurchaseByCategoryRequest.apply _))
    .out(jsonBody[Response[GetPurchaseByCategoryResponse]])
    .summary("Получение покупок по типу")
    .handleWithAuthorization(req =>
      userId => GetPurchaseByCategoryHandler(purchaseService)(req, userId)
    )

  val addPurchaseEndpiont = endpoint // добавить покупку
    .post
    .in("add_purchase")
    .in(jsonBody[AddPurchaseRequest])
    .out(jsonBody[Response[AddPurchaseResponse]])
    .summary("Добавление покупки")
    .handleWithAuthorization(req => userId => AddPurchaseHandler(purchaseService)(req, userId))

  override def httpRoutes(
    implicit
    serverOptions: Http4sServerOptions[Task]
  ): HttpRoutes[Task] = Http4sServerInterpreter.toRoutes(
    List(
      purchaseInfoEndpoint,
      getUserPurchasesEndpiont,
      getPurchasesByTypeEndpiont,
      addPurchaseEndpiont
    )
  )

  override def endPoints: List[Endpoint[_, Unit, _, _]] = List(
    purchaseInfoEndpoint.endpoint,
    getUserPurchasesEndpiont.endpoint,
    getPurchasesByTypeEndpiont.endpoint,
    addPurchaseEndpiont.endpoint
  )

}
