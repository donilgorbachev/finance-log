package ru.dreamteam.business.handlers.purchase.handlers

import ru.dreamteam.business.Purchase.{Comment, PurchaseCategory}
import ru.dreamteam.business.handlers.purchase._
import ru.dreamteam.business.services.purchases.PurchasesService
import ru.dreamteam.business.{Currency, Money, Purchase, User}
import ru.dreamteam.infrastructure.{MainEnv, MainTask}
import zio.ZIO

object PurchaseInfoHandler {

  def apply[R](
    purchasesService: PurchasesService[MainTask]
  )(req: PurchaseInfoRequest, userId: User.Id): ZIO[MainEnv, Throwable, PurchaseInfoResponse] =
    purchasesService.purchaseInfo(userId, Purchase.Id(req.purchaseId))
      .map(info => PurchaseInfoResponse(info))

}

object GetPurchasesHandler {

  def apply[R](
    purchasesService: PurchasesService[MainTask]
  )(req: GetPurchasesRequest, userId: User.Id): ZIO[MainEnv, Throwable, GetPurchasesResponse] =
    purchasesService.getPurchases(userId).map(purchases => GetPurchasesResponse(purchases))

}

object GetPurchaseByCategoryHandler {

  def apply[R](
    purchasesService: PurchasesService[MainTask]
  )(
    req: GetPurchaseByCategoryRequest,
    userId: User.Id
  ): ZIO[MainEnv, Throwable, GetPurchaseByCategoryResponse] =
    purchasesService.getPurchaseByCategory(
      userId,
      PurchaseCategory.parse(req.purchaseCategory)
    ).map(purchases => GetPurchaseByCategoryResponse(purchases))

}

object AddPurchaseHandler {

  def apply[R](
    purchasesService: PurchasesService[MainTask]
  )(req: AddPurchaseRequest, userId: User.Id): ZIO[MainEnv, Throwable, AddPurchaseResponse] =
    purchasesService.addPurchase(
      userId,
      Money(req.amount, Currency.parse(req.currency)),
      Comment(req.comment),
      PurchaseCategory.parse(req.category)
    ).map(info => AddPurchaseResponse(info))

}
